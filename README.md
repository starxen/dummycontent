# DummyContent

DummyContent is a Symfony Bundle that provides features that allow you to generate sample content for development
purposes

## Capability

- Generating images
- Lorem Ipsum generator
- Text processing
- Generating random things

## Installation

*composer require starxen/dummycontent*

## Usage

See [DOCUMENTATION.md](docs/DOCUMENTATION.md)
