<?php

declare(strict_types=1);

namespace StarXen\DummyContent\Service;

class TextTools
{
    public function sliceSentences(string $text, int $length): string
    {
        $sentences = preg_split('/(?<=[.?!])\s+/', $text, -1, PREG_SPLIT_NO_EMPTY);

        if (count($sentences) <= $length) {
            return $text;
        }

        return implode(' ', array_slice($sentences, 0, $length));
    }

    public function sliceWords(string $text, int $length): string
    {
        $words = explode(' ', trim($text));

        if (count($words) <= $length) {
            return $text;
        }

        return implode(' ', array_slice($words, 0, $length));
    }
}