<?php

declare(strict_types=1);

namespace StarXen\DummyContent\Service;

use DateTime;

class RandomTools
{
    public function randomDate(string $from = '-1 month', string $to = 'now'): DateTime
    {
        $start = new DateTime($from);
        $end = new DateTime($to);

        $randomTimestamp = mt_rand($start->getTimestamp(), $end->getTimestamp());

        $randomDate = new DateTime();
        $randomDate->setTimestamp($randomTimestamp);

        return $randomDate;
    }

    public function randomColor(): string
    {
        return sprintf('#%06X', mt_rand(0x000000, 0xFFFFFF));
    }
}