<?php

declare(strict_types=1);

namespace StarXen\DummyContent\Service;

use Symfony\Component\Filesystem\Filesystem;

class ImageGenerator
{
    public const string DEFAULT_BACKGROUND_COLOR = '#DDFFDD';
    public const string DEFAULT_FONT_COLOR = '#000000';

    public function generate(int $width, int $height, string $backgroundColor = self::DEFAULT_BACKGROUND_COLOR, string $fontColor = self::DEFAULT_FONT_COLOR): string
    {
        $image = imagecreatetruecolor($width, $height);

        [$r, $g, $b] = sscanf($backgroundColor, "#%02x%02x%02x");
        $backgroundColor = imagecolorallocate($image, $r, $g, $b);
        [$r, $g, $b] = sscanf($fontColor, "#%02x%02x%02x");
        $fontColor = imagecolorallocate($image, $r, $g, $b);

        imagefilledrectangle($image, 0, 0, $width - 1, $height - 1, $backgroundColor);

        $text = "{$width}px x {$height}px";
        $font = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'font' . DIRECTORY_SEPARATOR . 'OpenSansItalic.ttf';

        if ($height < 35 || $width < 180) {
            $fontSize = 8;
        } else {
            $fontSize = 20;
        }

        $centerX = $width / 2;
        $centerY = $height / 2;

        [$left, $bottom, $right, , , $top] = imageftbbox($fontSize, 0, $font, $text);

        $leftOffset = ($right - $left) / 2;
        $topOffset = ($bottom - $top) / 2;

        $x = $centerX - $leftOffset;
        $y = $centerY + $topOffset;

        imagettftext($image, $fontSize, 0, (int) $x, (int) $y, $fontColor, $font, $text);

        ob_start();
        imagepng($image);
        $content = ob_get_contents();
        ob_end_clean();
        imagedestroy($image);

        return $content;
    }

    public function generateAsBase64(int $width, int $height, string $backgroundColor = self::DEFAULT_BACKGROUND_COLOR, string $fontColor = self::DEFAULT_FONT_COLOR): string
    {
        return base64_encode($this->generate($width, $height, $backgroundColor, $fontColor));
    }

    public function generateAsTempFile(int $width, int $height, string $backgroundColor = self::DEFAULT_BACKGROUND_COLOR, string $fontColor = self::DEFAULT_FONT_COLOR): string
    {
        $filesystem = new Filesystem();

        $path = $filesystem->tempnam(sys_get_temp_dir(), 'dummycontent_', '.png');
        $filesystem->dumpFile($path, $this->generate($width, $height, $backgroundColor, $fontColor));

        return $path;
    }
}