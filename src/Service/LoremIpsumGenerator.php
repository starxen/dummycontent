<?php

declare(strict_types=1);

namespace StarXen\DummyContent\Service;

class LoremIpsumGenerator
{
    private const array LOREM_WORDS = ['lorem', 'ipsum', 'dolor', 'sit', 'amet', 'consectetur', 'adipiscing', 'elit', 'ut', 'ac', 'mauris', 'nunc', 'fusce', 'consequat', 'nibh', 'non', 'placerat', 'massa', 'ultricies', 'eget', 'integer', 'a', 'erat', 'commodo', 'vulputate', 'enim', 'et', 'scelerisque', 'nisi', 'vestibulum', 'euismod', 'ligula', 'nec', 'blandit', 'feugiat', 'quam', 'dui', 'congue', 'vel', 'rutrum', 'velit', 'mi', 'nulla', 'suspendisse', 'laoreet', 'imperdiet', 'ornare', 'bibendum', 'nam', 'gravida', 'condimentum', 'tortor', 'id', 'pretium', 'lobortis', 'at', 'sapien', 'eros', 'hendrerit', 'in', 'magna', 'quis', 'viverra', 'accumsan', 'nisl', 'nullam', 'dictum', 'augue', 'cras', 'vehicula', 'dictum praesent', 'tristique', 'volutpat', 'odio', 'iaculis', 'metus', 'pellentesque', 'tincidunt', 'ullamcorper', 'aliquam', 'neque', 'sodales', 'tellus', 'aliquet', 'mollis', 'maximus', 'phasellus', 'interdum', 'libero', 'suscipit', 'sed', 'porttitor', 'ultrices', 'risus', 'mattis', 'faucibus', 'diam', 'purus', 'urna'];

    public function generate(int $words): string
    {
        $lorem = '';
        $counter = 0;
        $chainLength = 0;
        while ($words > $counter) {
            $counter++;
            $chainLength++;
            if ($counter === 1) {
                $lorem .= ucfirst(self::LOREM_WORDS[$counter - 1]);
                continue;
            }
            if ($counter < 6) {
                $lorem .= ' ' . self::LOREM_WORDS[$counter - 1] . ($counter === 5 ? ',' : '');
                continue;
            }
            if ($chainLength < 5) {
                $lorem .= ' ' . self::LOREM_WORDS[rand(0, count(self::LOREM_WORDS) - 1)];
                continue;
            }
            if ($this->chance(5)) {
                $lorem .= ',';
                $chainLength = 0;
            } else if ($this->chance(15)) {
                $chainLength = 0;
                $lorem .= '. ' . ucfirst(self::LOREM_WORDS[rand(0, count(self::LOREM_WORDS) - 1)]);
                continue;
            }

            $lorem .= ' ' . self::LOREM_WORDS[rand(0, count(self::LOREM_WORDS) - 1)];
        }

        return str_replace([',.', '.,', ',,'], ['.', '.', '.'], $lorem . '.');
    }

    public function getLoremIpsumWords(): array
    {
        return self::LOREM_WORDS;
    }

    private function chance(int $percent): bool
    {
        return rand(0, 100) <= $percent;
    }
}