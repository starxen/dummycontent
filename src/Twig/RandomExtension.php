<?php

declare(strict_types=1);

namespace StarXen\DummyContent\Twig;

use DateTime;
use StarXen\DummyContent\Service\RandomTools;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class RandomExtension extends AbstractExtension
{
    public function __construct(private readonly RandomTools $randomTools)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('random_date', [$this, 'randomDate']),
            new TwigFunction('random_color', [$this, 'randomColor'])
        ];
    }

    public function randomDate(string $from = '-1 month', string $to = 'now'): DateTime
    {
        return $this->randomTools->randomDate($from, $to);
    }

    public function randomColor(): string
    {
        return $this->randomTools->randomColor();
    }
}
