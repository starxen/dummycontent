<?php

declare(strict_types=1);

namespace StarXen\DummyContent\Twig;

use StarXen\DummyContent\Service\ImageGenerator;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ImageExtension extends AbstractExtension
{
    public function __construct(readonly private ImageGenerator $imageGenerator)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('dummy_image', [$this, 'dummyImage'], ['is_safe' => ['html']])
        ];
    }

    public function dummyImage(int $width, int $height, string $class = '', string $backgroundColor = ImageGenerator::DEFAULT_BACKGROUND_COLOR, string $fontColor = ImageGenerator::DEFAULT_FONT_COLOR): string
    {
        $imageData = $this->imageGenerator->generateAsBase64($width, $height, $backgroundColor, $fontColor);
        $html = <<<HTML
<img src="data:image/png;base64, $imageData" alt="{$height}px x {$width}px" class="$class"/>
HTML;
        return $html;
    }

}
