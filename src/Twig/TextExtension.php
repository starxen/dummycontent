<?php

declare(strict_types=1);

namespace StarXen\DummyContent\Twig;

use StarXen\DummyContent\Service\LoremIpsumGenerator;
use StarXen\DummyContent\Service\TextTools;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TextExtension extends AbstractExtension
{

    public function __construct(
        private readonly TextTools $textTools,
        private readonly LoremIpsumGenerator $loremIpsumGenerator
    )
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('lorem_ipsum', [$this, 'loremIpsum'])
        ];
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('words', [$this, 'words']),
            new TwigFilter('sentences', [$this, 'sentences'])
        ];
    }

    public function sentences(string $text, int $length): string
    {
        return $this->textTools->sliceSentences($text, $length);
    }

    public function words(string $text, int $length): string
    {
        return $this->textTools->sliceWords($text, $length);
    }

    public function loremIpsum(int $words): string
    {
        return $this->loremIpsumGenerator->generate($words);
    }

}
