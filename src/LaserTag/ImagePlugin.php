<?php

declare(strict_types=1);

namespace StarXen\DummyContent\LaserTag;

use StarXen\DummyContent\Service\ImageGenerator;
use StarXen\LaserTag\LaserTag\AbstractPlugin;
use StarXen\LaserTag\LaserTag\LaserTag;
use StarXen\LaserTag\LaserTag\TagAttribute;

class ImagePlugin extends AbstractPlugin
{

    public function __construct(private readonly ImageGenerator $imageGenerator)
    {
    }

    public function getTags(): array
    {
        return [
            new LaserTag('dummyimg', [$this, 'dummyimg'], [
                new TagAttribute('w', true),
                new TagAttribute('h', true),
                new TagAttribute('bg_color'),
                new TagAttribute('font_color'),
                new TagAttribute('class'),
            ]),
        ];
    }

    public function dummyimg(): string
    {
        $width = (int) $this->getAttribute('w');
        $height = (int) $this->getAttribute('h');
        $backgroundColor = $this->getAttribute('bg_color') ?? ImageGenerator::DEFAULT_BACKGROUND_COLOR;
        $fontColor = $this->getAttribute('font_color') ?? ImageGenerator::DEFAULT_FONT_COLOR;

        $imageData = $this->imageGenerator->generateAsBase64($width, $height, $backgroundColor, $fontColor);
        $class = $this->getAttribute('class');

        return <<<HTML
<img src="data:image/png;base64, $imageData" alt="{$height}px x {$width}px" class="$class"/>
HTML;
    }

}
