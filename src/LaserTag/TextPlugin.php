<?php

declare(strict_types=1);

namespace StarXen\DummyContent\LaserTag;

use StarXen\DummyContent\Service\LoremIpsumGenerator;
use StarXen\LaserTag\LaserTag\AbstractPlugin;
use StarXen\LaserTag\LaserTag\LaserTag;
use StarXen\LaserTag\LaserTag\TagAttribute;

class TextPlugin extends AbstractPlugin
{

    public function __construct(private readonly LoremIpsumGenerator $loremIpsumGenerator)
    {
    }

    public function getTags(): array
    {
        return [
            new LaserTag('lorem', [$this, 'lorem'], [new TagAttribute('words', true), new TagAttribute('class')]),
        ];
    }

    public function lorem(): string
    {
        $words = (int) $this->getAttribute('words');
        $class = $this->getAttribute('class');

        return <<<HTML
<span class="$class">{$this->loremIpsumGenerator->generate($words)}</span>
HTML;
    }

}
