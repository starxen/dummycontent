<?php

declare(strict_types=1);

namespace StarXen\DummyContent\LaserTag;

use StarXen\DummyContent\Service\RandomTools;
use StarXen\LaserTag\LaserTag\AbstractPlugin;
use StarXen\LaserTag\LaserTag\LaserTag;
use StarXen\LaserTag\LaserTag\TagAttribute;

class RandomPlugin extends AbstractPlugin
{

    public function __construct(private readonly RandomTools $randomTools)
    {
    }

    public function getTags(): array
    {
        return [
            new LaserTag('random_date', [$this, 'randomDate'], [new TagAttribute('format'), new TagAttribute('from'), new TagAttribute('to')]),
        ];
    }

    public function randomDate(): string
    {
        $from = $this->getAttribute('from') ?? '-1 month';
        $to = $this->getAttribute('to') ?? 'now';

        return $this->randomTools->randomDate($from, $to)->format($this->getAttribute('format') ?? 'd.m.Y');
    }

}
