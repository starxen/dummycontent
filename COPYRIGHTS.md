## Licences

### Font - OpenSans

This bundle utilizes the Open Sans font, which is licensed under the SIL Open Font License (OFL).

#### Open Sans

Open Sans is a free font from the Google Fonts project. It is distributed under the SIL Open Font License (OFL), which
means it can be used, modified, and distributed in both personal and commercial projects, as long as the terms of this
license are followed.

For more information about the SIL Open Font License, please
visit [https://openfontlicense.org](https://openfontlicense.org).

#### Notice

All copyrights to the Open Sans font belong to their respective owners.


