# DummyContent documentation

## Installation

*composer require starxen/dummycontent*

Make sure the bundle is correctly added in your project

```php
// config/bundles.php
StarXen\DummyContent\DummyContentBundle::class => ['all' => true]
```

## Services

- [ImageGenerator](services/IMAGE_GENERATOR.md)
- [LoremIpsumGenerator](services/LOREM_IPSUM_GENERATOR.md)
- [RandomTools](services/RANDOM_TOOLS.md)
- [TextTools](services/TEXT_TOOLS.md)
