# RandomTools

```php
// src/Service/YourService.php
<?php

declare(strict_types=1);

namespace App\Service;

use StarXen\DummyContent\Service\RandomTools;

readonly class YourService
{

    public function __construct(private RandomTools $randomTools)
    {
    }

}
```

## *randomDate*

### PHP Usage

*PHP definition*

```php
public function randomDate(string $from = '-1 month', string $to = 'now'): DateTime
```

*Params*

- int $from - [*optional*] The start date or start date modifier
- int $to - [*optional*] The end date or end date modifier

*Return*

- DateTime - Random date

*Example*

```php
echo $this->randomTools->randomDate('01-01-2010', '01-01-2025')->format('d/m/Y H:i:s');
```

*Output*

```text
17/05/2013 04:07:46
```

### Twig Usage

*Syntax*

```twig
{{ random_date('01-01-2010', '01-01-2025') }}
```

*PHP definition*

```php
public function randomDate(string $from = '-1 month', string $to = 'now'): DateTime
```

*Params*

- int $from - [*optional*] The start date or start date modifier
- int $to - [*optional*] The end date or end date modifier

*Return*

- DateTime - Random date

*Example*

```twig
{{ random_date('01-01-2010', '01-01-2025').format('d/m/Y H:i:s') }}
```

*Output*

```html
16/09/2021 04:35:22
```

### LaserTag Usage

*Example*

```lasertag
[random_date from="01-01-2010" to="01-01-2025" format="d/m/Y H:i:s"/]
```

*Attributes*

- from - [*optional*] The start date or start date modifier
- to - [*optional*] The end date or end date modifier
- format - [*optional*] Output date format | *Default:* 'd.m.Y'

*Return*

- Formatted random date

*Output*

```html
02/02/2011 21:48:16
```

---

## *randomColor*

### PHP Usage

*PHP definition*

```php
public function randomColor(): string
```

*Return*

- string - random color in HEX

*Example*

```php
echo $this->randomTools->randomColor();
```

*Output*

```text
#F78A67
```

### Twig Usage

*Example*

```twig
{{ random_color() }}
```

*PHP definition*

```php
public function randomColor(): string
```

*Return*

- string - random color in HEX

*Output*

```html
#10D255
```

---
 