# ImageGenerator

```php
// src/Service/YourService.php
<?php

declare(strict_types=1);

namespace App\Service;

use StarXen\DummyContent\Service\ImageGenerator;

readonly class YourService
{

    public function __construct(private ImageGenerator $imageGenerator)
    {
    }

}
```

## *generate*

### PHP Usage

*PHP definition*

```php
public function generate(int $width, int $height, string $backgroundColor = self::DEFAULT_BACKGROUND_COLOR, string $fontColor = self::DEFAULT_FONT_COLOR): string
```

*Params*

- int $width - [*required*] Image width
- int $height - [*required*] Image height
- string $backgroundColor - [*optional*] Background Color as HEX eg. '#FF00FF'
- string $fontColor - [*optional*] Font Color as HEX eg. '#FF00FF'

*Return*

- string - Generated PNG content

*Example*

```php
$content = $this->imageGenerator->generate(100, 200, '#000000', '#FFFFFF');
```

### Twig Usage

*Example*

```twig
{{ dummy_image(100, 200, 'css_class', '#000000', '#FFFFFF') }}
```

*PHP definition*

```php
public function dummyImage(int $width, int $height, string $class = '', string $backgroundColor = ImageGenerator::DEFAULT_BACKGROUND_COLOR, string $fontColor = ImageGenerator::DEFAULT_FONT_COLOR): string
```

*Params*

- int $width - [*required*] Image width
- int $height - [*required*] Image height
- string $class - [*optional*] Custom HTML class attribute
- string $backgroundColor - [*optional*] Background Color as HEX eg. '#FF00FF'
- string $fontColor - [*optional*] Font Color as HEX eg. '#FF00FF'

*Return*

- string - HTML img tag with generated image as base64

*Output*

```html
<img src="data:image/png;base64, *image_content_as_base64*" alt="200px x 100px" class="css_class">
```

### LaserTag Usage

*Example*

```lasertag
[dummyimg w="100" h="200" class="css_class" bg_color="#000000" font_color="#FFFFFF"/]
```

*Attributes*

- w - [*required*] Image width
- h - [*required*] Image height
- class - [*optional*] Custom HTML class attribute
- bg_color - [*optional*] Background Color as HEX eg. "#FF00FF"
- font_color - [*optional*] Font Color as HEX eg. "#FF00FF"

*Return*

- HTML img tag with generated image as base64

*Output*

```html
<img src="data:image/png;base64, *image_content_as_base64*" alt="200px x 100px" class="css_class">
```

---

## *generateAsBase64*

### PHP Usage

*PHP definition*

```php
public function generateAsBase64(int $width, int $height, string $backgroundColor = self::DEFAULT_BACKGROUND_COLOR, string $fontColor = self::DEFAULT_FONT_COLOR): string
```

*Params*

- int $width - [*required*] Image width
- int $height - [*required*] Image height
- string $backgroundColor - [*optional*] Background Color as HEX eg. '#FF00FF'
- string $fontColor - [*optional*] Font Color as HEX eg. '#FF00FF'

*Return*

- string - Generated PNG content encoded in base64

*Example*

```php
$imgBase64 = $this->imageGenerator->generateAsBase64(100, 200, '#000000', '#FFFFFF');
```

---

## *generateAsTempFile*

### PHP Usage

*PHP definition*

```php
public function generateAsTempFile(int $width, int $height, string $backgroundColor = self::DEFAULT_BACKGROUND_COLOR, string $fontColor = self::DEFAULT_FONT_COLOR): string
```

*Params*

- int $width - [*required*] Image width
- int $height - [*required*] Image height
- string $backgroundColor - [*optional*] Background Color as HEX eg. '#FF00FF'
- string $fontColor - [*optional*] Font Color as HEX eg. '#FF00FF'

*Return*

- string - The temporary file path of the generated image

*Example*

```php
$tempFilePath = $this->imageGenerator->generateAsTempFile(100, 200, '#000000', '#FFFFFF');
```

---