# LoremIpsumGenerator

```php
// src/Service/YourService.php
<?php

declare(strict_types=1);

namespace App\Service;

use StarXen\DummyContent\Service\LoremIpsumGenerator;

readonly class YourService
{

    public function __construct(private LoremIpsumGenerator $loremIpsumGenerator)
    {
    }

}
```

## *generate*

### PHP Usage

*PHP definition*

```php
public function generate(int $words): string
```

*Params*

- int $words - [*required*] Number of words to generate

*Return*

- string - Generated Lorem ipsum

*Example*

```php
$loremIpsum = $this->loremIpsumGenerator->generate(20);
```

*Output*

```text
Lorem ipsum dolor sit amet, suscipit. Tellus in laoreet commodo aliquet, adipiscing eros libero dictum praesent amet mauris, congue pellentesque tristique.
```

### Twig Usage

*Example*

```twig
{{ lorem_ipsum(20) }}
```

*PHP definition*

```php
public function loremIpsum(int $words): string
```

*Params*

- int $words - [*required*] Number of words to generate

*Return*

- string - Generated Lorem ipsum

*Output*

```html
Lorem ipsum dolor sit amet, a imperdiet diam, lorem purus ipsum tincidunt eget mauris neque gravida sed nisi accumsan tincidunt.
```

### LaserTag Usage

*Example*

```lasertag
[lorem words="20" class="css_class"/]
```

*Attributes*

- words - [*required*] Number of words to generate
- class - [*optional*] Custom HTML class attribute

*Return*

- HTML span with generated Lorem ipsum

*Output*

```html
<span class="css_class">Lorem ipsum dolor sit amet, sodales congue non vestibulum, pellentesque tellus ac condimentum aliquet libero feugiat in maximus dui nec.</span>
```

---

## *getLoremIpsumWords*

### PHP Usage

*PHP definition*

```php
public function getLoremIpsumWords(): array
```

*Return*

- array - **string[]** - Lorem Ipsum words

*Example*

```php
$loremWords = $this->loremIpsumGenerator->getLoremIpsumWords();
```

---
 