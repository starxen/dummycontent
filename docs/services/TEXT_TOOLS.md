# TextTools

```php
// src/Service/YourService.php
<?php

declare(strict_types=1);

namespace App\Service;

use StarXen\DummyContent\Service\TextTools;

readonly class YourService
{

    public function __construct(private TextTools $textTools)
    {
    }

}
```

## *sliceSentences*

### PHP Usage

*PHP definition*

```php
public function sliceSentences(string $text, int $length): string
```

*Params*

- string $text - [*required*] Text to slice
- int $length - [*required*] Number of sentences

*Return*

- string - Sliced text

*Example*

```php
$text = 'First sentence. Second sentence. Third sentence';
echo $this->textTools->sliceSentences($text, 2);
```

*Output*

```text
First sentence. Second sentence.
```

### Twig Usage

*Syntax*

```twig
{{ string | sentences(2) }}
```

*PHP definition*

```php
public function sentences(string $text, int $length): string
```

*Params*

- string $text - [*required*] Text to slice
- int $length - [*required*] Number of sentences

*Return*

- string - Sliced text

*Example*

```twig
{{ 'First sentence. Second sentence. Third sentence' | sentences(2) }}
```

*Output*

```html
First sentence. Second sentence.
```

---

## *sliceWords*

### PHP Usage

*PHP definition*

```php
public function sliceWords(string $text, int $length): string
```

*Params*

- string $text - [*required*] Text to slice
- int $length - [*required*] Number of words

*Return*

- string - Sliced text

*Example*

```php
$text = 'Lorem ipsum dolor sit amet, laoreet dictum praesent. Aliquet condimentum ultricies nibh';
echo $this->textTools->sliceWords($text, 8);
```

*Output*

```text
Lorem ipsum dolor sit amet, laoreet dictum praesent.
```

### Twig Usage

*Syntax*

```twig
{{ string | words(2) }}
```

*PHP definition*

```php
public function words(string $text, int $length): string
```

*Params*

- string $text - [*required*] Text to slice
- int $length - [*required*] Number of words

*Return*

- string - Sliced text

*Example*

```twig
{{ 'Lorem ipsum dolor sit amet, vulputate interdum dictum sit libero imperdiet ultrice.' | words(10) }}
```

*Output*

```html
Lorem ipsum dolor sit amet, vulputate interdum dictum sit libero
```

---
 